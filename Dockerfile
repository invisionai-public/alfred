# As of 2024-07-04, rust:1.79.0-alpine3.20, buildx on arm64 segfaults sometimes, so we use an older version for now
FROM rust:1.78.0-alpine3.19 AS builder

RUN apk add --no-cache musl-dev
COPY . /code
RUN cd /code && cargo build --release


FROM alpine:3.20.1
COPY --from=builder /code/target/release/alfred /bin/alfred

ENTRYPOINT ["alfred"]
