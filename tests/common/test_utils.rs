use more_asserts::*;
use std::fs;
use std::io::Write;
use std::path::PathBuf;
use tempfile::TempDir;

#[derive(Debug, Clone)]
pub struct ComposeOpts {
    sys_ptrace: bool,
    host_pid: bool,
}

impl ComposeOpts {
    pub fn all_enabled() -> Self {
        Self { sys_ptrace: true, host_pid: true }
    }

    pub fn sys_ptrace(mut self, on: bool) -> Self {
        self.sys_ptrace = on;
        self
    }

    pub fn host_pid(mut self, on: bool) -> Self {
        self.host_pid = on;
        self
    }
}

/// Creates docker-compose.yml in temporary directory
/// with given labels.
pub fn make_compose_project_dir(labels: &str, opts: ComposeOpts) -> TempDir {
    let tempdir = TempDir::new().expect("Could not create temp dir");

    let mut compose_fname = tempdir.path().to_path_buf();
    compose_fname.push("docker-compose.yml");

    let mut compose_file =
        std::fs::File::create(compose_fname).expect("Could not create compose file");

    compose_file
        .write(
            b"  
services:
  samplecontainer:
    build:
      dockerfile_inline: |
        FROM bash
        # so we have a fully-functional date command
        RUN apk add coreutils --upgrade
    init: true
    restart: always
    command: sleep inf
    volumes:
      - ${OUT_DIR}:/out
    labels:
    ",
        )
        .expect("Error writing compose file");

    // add labels, but indent correctly first
    for line in labels.split("\n").into_iter() {
        write!(compose_file, "      {}\n", line.trim()).unwrap();
    }

    compose_file
        .write(
            b"
  alfred:
    build:
      # socat handy to forward tcp to unix socket
      dockerfile_inline: |
        FROM debian:12
        RUN apt update && apt install -y socat

    init: true  # to pass signals to main process, especially to pass sigterm
    command: |
        bash -c \"
            sleep ${ALFRED_PRE_DELAY:-0s} && 
            /target/alfred --prometheus-port ${PROMETHEUS_PORT} --docker-socket ${ALFRED_DOCKER_SOCKET:-unix:///var/run/docker.sock}
        \"

    ports:
      - \"${PROMETHEUS_PORT}:${PROMETHEUS_PORT}\"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ${TARGET_DIR}:/target
",
        )
        .expect("Error writing compose file");

    if opts.host_pid {
        compose_file
            .write(
                b"
    pid: host
",
            )
            .expect("Error writing file");
    }

    if opts.sys_ptrace {
        compose_file
            .write(
                b"
    cap_add:
      - SYS_PTRACE
",
            )
            .expect("Error writing file");
    }

    tempdir
}

#[derive(Default, Debug)]
pub struct LogEntry {
    pub timestamp: f64,
    pub minute: i32,
    pub second: i32,
}

// parse each line of a log file containing
//    "{timestamp} {mm}:{ss}"
pub fn parse_logfile(fname: &PathBuf) -> Vec<LogEntry> {
    let data = match fs::read_to_string(fname) {
        Ok(data) => data,
        Err(err) => panic!("Error reading file {:?}: {}", fname, err),
    };

    let mut parsed_lines: Vec<LogEntry> = vec![];
    for line in data.split('\n') {
        if line.trim().len() == 0 {
            continue;
        }
        let mut entry = LogEntry { ..Default::default() };
        text_io::scan!(line.bytes() => "{} {}:{}", entry.timestamp, entry.minute, entry.second);
        parsed_lines.push(entry);
    }
    parsed_lines
}

pub fn check_logfile(entries: &Vec<LogEntry>, interval_sec: i64) {
    assert_ge!(entries.len(), 2, "Must have at least two entries");
    for (idx, entry) in entries.iter().enumerate() {
        assert!(entry.second as i64 % interval_sec == 0, "Wrong second value");
        if idx > 0 {
            assert!(
                (entry.timestamp - entries[idx - 1].timestamp).round() as i64 == interval_sec,
                "Wrong interval"
            );
        }
    }
}

/// Find metric in prometheus' scraped lines or crash otherwise
pub fn find_metric(lines: &Vec<String>, name: &str) -> Option<f64> {
    for line in lines {
        if line.starts_with(name) {
            let tokens: Vec<&str> = line.split_ascii_whitespace().into_iter().collect();
            assert_eq!(tokens.len(), 2, "Unexpected number of tokens");

            return Some(tokens[1].parse().expect("Could not convert to float"));
        }
    }

    None
}

pub fn assert_metric_eq(lines: &Vec<String>, metric_name: &str, expected_value: f64) {
    match find_metric(lines, metric_name) {
        None => panic!("Could not find metric with name {}", metric_name),
        Some(metric_value) => {
            assert_eq!(metric_value, expected_value, "Unexpected value for {}", metric_name)
        }
    };
}

pub fn assert_metric_gt(lines: &Vec<String>, metric_name: &str, expected_value: f64) {
    match find_metric(lines, metric_name) {
        None => panic!("Could not find metric with name {}", metric_name),
        Some(metric_value) => {
            assert_gt!(metric_value, expected_value, "Unexpected value for {}", metric_name)
        }
    };
}

pub fn assert_metric_ge(lines: &Vec<String>, metric_name: &str, expected_value: f64) {
    match find_metric(lines, metric_name) {
        None => panic!("Could not find metric with name {}", metric_name),
        Some(metric_value) => {
            assert_ge!(metric_value, expected_value, "Unexpected value for {}", metric_name)
        }
    };
}
