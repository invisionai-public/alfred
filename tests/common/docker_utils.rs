use crate::common::test_utils;

use crate::common::test_utils::find_metric;
use anyhow::Context;
use assert_cmd::Command;
use std::{
    net::TcpListener,
    path::PathBuf,
    thread::sleep,
    time::{Duration, SystemTime},
};
use tempfile::TempDir;

/// Get target directory, where binaries are compiled
// Adapted from
// https://github.com/rust-lang/cargo/blob/485670b3983b52289a2f353d589c57fae2f60f82/tests/testsuite/support/mod.rs#L507
fn target_dir() -> PathBuf {
    std::env::current_exe()
        .ok()
        .map(|mut path| {
            path.pop();
            if path.ends_with("deps") {
                path.pop();
            }
            path
        })
        .unwrap()
}

/// There's a very small chance this port may be re-used befroe we start docker compose,
/// but is still very unlikely
fn find_available_tcp_port() -> u16 {
    let listener = TcpListener::bind("127.0.0.1:0").expect("Could not bind to random port");
    listener.local_addr().expect("Could not get local_addr").port()
}

/// To make sure we stop docker compose after we are done
/// And that we keep track of the env vars we need to pass for our tests
pub struct DockerComposeProject {
    name: String,
    out_dir: TempDir,
    compose_dir: PathBuf,
    prometheus_port: u16,
    // force alfred to use this socket instead, if not empty
    docker_vars: DockerVars,
}

pub struct DockerVars {
    pub alfred_docker_socket: String,
    pub alfred_pre_delay: String,
}

impl DockerVars {
    pub fn new_empty() -> Self {
        Self { alfred_docker_socket: "".into(), alfred_pre_delay: "".into() }
    }

    pub fn alfred_pre_delay(mut self, value: &str) -> Self {
        self.alfred_pre_delay = value.into();
        self
    }

    pub fn alfred_docker_socket(mut self, value: &str) -> Self {
        self.alfred_docker_socket = value.into();
        self
    }
}

impl DockerComposeProject {
    pub fn new(name: &str, compose_dir: PathBuf, docker_vars: DockerVars) -> DockerComposeProject {
        // make sure we are somewhere that docker can mount, and target_dir()
        // is inside our absolute path for tests
        let mut temp_parent = target_dir().to_path_buf();
        temp_parent.push("temp");
        std::fs::create_dir_all(temp_parent.clone()).expect("Error creating temp parent dir");

        Self {
            name: name.to_string(),
            out_dir: TempDir::new_in(temp_parent).expect("Cannot create temp dir"),
            compose_dir,
            prometheus_port: find_available_tcp_port(),
            docker_vars,
        }
    }

    pub fn docker(&self, ops: Vec<&'static str>) {
        let mut cmd = Command::new("docker");
        cmd.env("TARGET_DIR", target_dir())
            .env("OUT_DIR", self.out_dir.path())
            .env("PROMETHEUS_PORT", self.prometheus_port.to_string())
            .env("ALFRED_DOCKER_SOCKET", self.docker_vars.alfred_docker_socket.clone())
            .env("ALFRED_PRE_DELAY", self.docker_vars.alfred_pre_delay.clone())
            .arg("compose")
            .arg("--project-directory")
            .arg(self.compose_dir.as_os_str())
            .arg("--project-name")
            .arg(self.name.clone())
            .args(ops);
        cmd.assert().success();
    }

    pub fn up(&self) {
        self.docker(vec!["up", "-d"]);
    }

    pub fn down(&self) {
        self.docker(vec!["down"]);
    }

    pub fn service_down(&self, service: &'static str) {
        self.docker(vec!["down", service]);
    }

    pub fn service_up(&self, service: &'static str) {
        self.docker(vec!["up", "-d", service]);
    }

    pub fn out_dir(&self) -> &TempDir {
        &self.out_dir
    }

    /// Scrape data from prometheus exporter
    pub fn scrape_or_panic(&self) -> Vec<String> {
        self.scrape().expect("Could not scrape alfred")
    }

    pub fn scrape(&self) -> anyhow::Result<Vec<String>> {
        let resp = reqwest::blocking::get(format!("http://127.0.0.1:{}", self.prometheus_port))?;

        let body = resp.text().context("Could not read request body")?;

        Ok(body.split('\n').into_iter().map(|x| x.trim().to_string()).collect())
    }

    /// Returns true if alfred is up and at least one job was registered
    pub fn wait_for_alfred(&self, timeout: Duration) -> bool {
        let deadline = SystemTime::now() + timeout;
        loop {
            if let Ok(scraped_lines) = self.scrape() {
                if let Some(value) = find_metric(&scraped_lines, "alfred_job_registered") {
                    if value == 1.0 {
                        return true;
                    }
                }
            }

            if SystemTime::now() > deadline {
                return false;
            }
            sleep(Duration::from_millis(100));
        }
    }
}

impl Drop for DockerComposeProject {
    fn drop(&mut self) {
        self.down();
        self.docker(vec!["rm"]);
    }
}
