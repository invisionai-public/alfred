use more_asserts::*;
use rstest::rstest;
use std::time::Duration;

mod common;

use common::docker_utils::{DockerComposeProject, DockerVars};

use common::test_utils::*;

#[rstest]
#[case::cron("cron", "*/2 * * * * *", "*/3 * * * * *")]
#[case::every("every", "@every 2s", "@every 3s")]
fn two_short_parallel_jobs_succeed(
    #[case] test_type: &str, #[case] schedule1: &str, #[case] schedule2: &str,
) {
    let compose_dir = make_compose_project_dir(
        format!(
            "
- alfred.enabled=true
- alfred.job-exec.job1.schedule={schedule1}
- alfred.job-exec.job1.command=bash -c \"date +'%s %M:%S' >> /out/job1.log\"
- alfred.job-exec.job1.no-overlap=true

- alfred.job-exec.job2.schedule={schedule2}
- alfred.job-exec.job2.command=bash -c \"date +'%s %M:%S' >> /out/job2.log\"
- alfred.job-exec.job2.no-overlap=false
"
        )
        .as_str(),
        ComposeOpts::all_enabled(),
    );
    let compose = DockerComposeProject::new(
        format!("two_parallel_jobs_succeed_{test_type}").as_str(),
        compose_dir.path().to_path_buf(),
        DockerVars::new_empty(),
    );
    compose.up();
    assert!(compose.wait_for_alfred(Duration::from_secs(1)), "Alfred not responding");

    std::thread::sleep(Duration::from_secs(10));

    let scraped_lines = compose.scrape_or_panic();

    // check per-job metrics
    for job_name in ["job1", "job2"] {
        assert_metric_eq(
            &scraped_lines,
            format!(
                r#"alfred_job_num_failed_runs{{container="samplecontainer",task="{}"}}"#,
                job_name
            )
            .as_str(),
            0.,
        );
        assert_metric_eq(
            &scraped_lines,
            format!(r#"alfred_job_registered{{container="samplecontainer",task="{}"}}"#, job_name)
                .as_str(),
            1.,
        );
        assert_metric_gt(
            &scraped_lines,
            format!(r#"alfred_job_num_ok_runs{{container="samplecontainer",task="{}"}}"#, job_name)
                .as_str(),
            2.,
        );
    }

    // check global metrics
    // when counters have zero value, they are not exported
    assert!(find_metric(&scraped_lines, "alfred_num_failed_jobs").is_none());
    assert!(find_metric(&scraped_lines, "alfred_num_system_errors").is_none());
    assert_metric_gt(&scraped_lines, "alfred_num_launched_jobs", 4.);
    assert_metric_gt(&scraped_lines, "alfred_num_succeeded_jobs", 4.);
    assert_metric_eq(&scraped_lines, "alfred_num_register_errors", 0.);

    compose.down();

    if test_type == "cron" {
        // read files and verify expected time values
        for (job_name, interval_sec) in vec![("job1.log", 2), ("job2.log", 3)] {
            let mut log_fname = compose.out_dir().path().to_path_buf();
            log_fname.push(job_name);
            check_logfile(&parse_logfile(&log_fname), interval_sec);
        }
    }
}

#[rstest]
#[case::cron("cron", "*/2 * * * * *", "*/3 * * * * *")]
#[case::every("every", "@every 2s", "@every 3s")]
fn job1_ok_job2_fails(#[case] test_type: &str, #[case] schedule1: &str, #[case] schedule2: &str) {
    let compose_dir = make_compose_project_dir(
        format!(
            "
- alfred.enabled=true
- alfred.job-exec.job1.schedule={schedule1}
- alfred.job-exec.job1.command=bash -c \"date +'%s %M:%S' >> /out/job1.log\"
- alfred.job-exec.job1.no-overlap=true

- alfred.job-exec.job2.schedule={schedule2}
- alfred.job-exec.job2.command=some bad command
- alfred.job-exec.job2.no-overlap=true
"
        )
        .as_str(),
        ComposeOpts::all_enabled(),
    );

    let compose = DockerComposeProject::new(
        format!("job1_ok_job2_fails_{test_type}").as_str(),
        compose_dir.path().to_path_buf(),
        DockerVars::new_empty(),
    );

    compose.up();
    assert!(compose.wait_for_alfred(Duration::from_secs(1)), "Alfred not responding");

    std::thread::sleep(Duration::from_secs(10));

    let scraped_lines = compose.scrape_or_panic();

    // check per-job metrics
    for job_name in vec!["job1", "job2"] {
        assert_metric_eq(
            &scraped_lines,
            format!(r#"alfred_job_registered{{container="samplecontainer",task="{}"}}"#, job_name)
                .as_str(),
            1.,
        );
    }

    // job 1 all good, no fails
    assert_metric_gt(
        &scraped_lines,
        r#"alfred_job_num_ok_runs{container="samplecontainer",task="job1"}"#,
        1.,
    );
    assert_metric_eq(
        &scraped_lines,
        r#"alfred_job_num_failed_runs{container="samplecontainer",task="job1"}"#,
        0.,
    );

    // job 2 all failed
    // when counters have zero value, they are not exported
    assert_metric_eq(
        &scraped_lines,
        r#"alfred_job_num_ok_runs{container="samplecontainer",task="job2"}"#,
        0.,
    );
    assert_metric_gt(
        &scraped_lines,
        r#"alfred_job_num_failed_runs{container="samplecontainer",task="job2"}"#,
        2.,
    );

    // check global metrics
    // when counters have zero value, they are not exported
    assert!(find_metric(&scraped_lines, "alfred_num_system_errors").is_none());
    assert_metric_gt(&scraped_lines, "alfred_num_failed_jobs", 2.);
    assert_metric_gt(&scraped_lines, "alfred_num_launched_jobs", 2.);
    assert_metric_gt(&scraped_lines, "alfred_num_succeeded_jobs", 2.);
    assert_metric_eq(&scraped_lines, "alfred_num_register_errors", 0.);

    compose.down();

    if test_type == "cron" {
        // read files and verify expected time values
        for (job_name, interval_sec) in vec![("job1.log", 2)] {
            let mut log_fname = compose.out_dir().path().to_path_buf();
            log_fname.push(job_name);
            check_logfile(&parse_logfile(&log_fname), interval_sec);
        }
    }
}

#[rstest]
#[case::cron("cron", "*/2 * * * * *")]
#[case::every("every", "@every 2s")]
fn job_that_runs_forever_with_no_overlap_gets_spawned_only_once(
    #[case] test_type: &str, #[case] schedule: &str,
) {
    let compose_dir = make_compose_project_dir(
        format!(
            "
- alfred.enabled=true
- alfred.job-exec.foreverjob.schedule={schedule}
- alfred.job-exec.foreverjob.command=bash -c \"date +'%s %M:%S' >> /out/log.txt && sleep inf\"
- alfred.job-exec.foreverjob.no-overlap=true
"
        )
        .as_str(),
        ComposeOpts::all_enabled(),
    );

    let compose = DockerComposeProject::new(
        format!("job_runs_forever_spawned_only_once_{test_type}").as_str(),
        compose_dir.path().to_path_buf(),
        DockerVars::new_empty(),
    );
    compose.up();
    assert!(compose.wait_for_alfred(Duration::from_secs(1)), "Alfred not responding");

    std::thread::sleep(Duration::from_secs(10));

    let scraped_lines = compose.scrape_or_panic();

    assert_metric_eq(
        &scraped_lines,
        r#"alfred_job_registered{container="samplecontainer",task="foreverjob"}"#,
        1.,
    );

    // job hasn't finished
    assert_metric_eq(
        &scraped_lines,
        r#"alfred_job_num_ok_runs{container="samplecontainer",task="foreverjob"}"#,
        0.,
    );

    assert_metric_eq(
        &scraped_lines,
        r#"alfred_job_num_failed_runs{container="samplecontainer",task="foreverjob"}"#,
        0.,
    );

    // verify start time metric
    let log_start_time = {
        let mut log_fname = compose.out_dir().path().to_path_buf();
        log_fname.push("log.txt");
        let contents = parse_logfile(&log_fname);
        assert_eq!(contents.len(), 1, "Expected only one line in log file but got multiple");

        contents[0].timestamp
    };

    let metric_start_time = find_metric(
        &scraped_lines,
        r#"alfred_oldest_running_job_start_time{container="samplecontainer",task="foreverjob"}"#,
    )
    .expect("Could not get job start time metric");

    // launching the scheduler may take some time, give some leeway
    assert_gt!(metric_start_time, log_start_time - 0.5, "oldest job start metric out of bounds");
    assert_lt!(metric_start_time, log_start_time + 1.0, "oldest job start metric out of bounds");

    // check global metrics
    // when counters have zero value, they are not exported
    assert!(find_metric(&scraped_lines, "alfred_num_system_errors").is_none());
    assert!(find_metric(&scraped_lines, "alfred_num_failed_jobs").is_none());
    assert_metric_eq(&scraped_lines, "alfred_num_launched_jobs", 1.);
    assert!(find_metric(&scraped_lines, "alfred_num_succeeded_jobs").is_none());
    assert_metric_eq(&scraped_lines, "alfred_num_register_errors", 0.);

    compose.down();

    // read log file to verify it was only run once
    {
        let mut log_fname = compose.out_dir().path().to_path_buf();
        log_fname.push("log.txt");
        let contents = parse_logfile(&log_fname);
        assert_eq!(contents.len(), 1, "Expected only one line in log file but got multiple");
    }
}

#[test]
fn no_overlap_every_job_launches_job_immediately_after_blocking_job_terminates() {
    let compose_dir = make_compose_project_dir(
        "
- alfred.enabled=true
# two jobs: one finishes instantly, the other one sleeps a bit more than the schedule time
- alfred.job-exec.refjob.schedule=@every 3s
- alfred.job-exec.refjob.command=bash -c \"date +'%s.%N %M:%S' >> /out/log1.txt\"
- alfred.job-exec.refjob.no-overlap=true

# will start at t=3 seconds after alfred starts up, sleep for 5 seconds,
# succeed at t=8 seconds, and launch a new one right after.
# Then, the delta between both jobs start time should be around 5 seconds,
# if the 2nd one started immediately. Otherwise it'd be 6.
- alfred.job-exec.blockedjob.schedule=@every 3s
- alfred.job-exec.blockedjob.command=bash -c \"date +'%s.%N %M:%S' >> /out/log2.txt && sleep 5\"
- alfred.job-exec.blockedjob.no-overlap=true
",
        ComposeOpts::all_enabled(),
    );

    let compose = DockerComposeProject::new(
        "every_no_overlap_launches_job_immediately_after_blocking_job_terminates",
        compose_dir.path().to_path_buf(),
        DockerVars::new_empty(),
    );
    compose.up();
    assert!(compose.wait_for_alfred(Duration::from_secs(1)), "Alfred not responding");

    std::thread::sleep(Duration::from_secs(10));

    let scraped_lines = compose.scrape_or_panic();

    for job_name in ["refjob", "blockedjob"] {
        assert_metric_eq(
            &scraped_lines,
            format!(
                r#"alfred_job_num_failed_runs{{container="samplecontainer",task="{}"}}"#,
                job_name
            )
            .as_str(),
            0.,
        );
        assert_metric_eq(
            &scraped_lines,
            format!(r#"alfred_job_registered{{container="samplecontainer",task="{}"}}"#, job_name)
                .as_str(),
            1.,
        );
    }

    assert_metric_eq(
        &scraped_lines,
        r#"alfred_job_num_ok_runs{container="samplecontainer",task="blockedjob"}"#,
        1.,
    );

    assert_metric_eq(
        &scraped_lines,
        r#"alfred_job_num_ok_runs{container="samplecontainer",task="refjob"}"#,
        3.,
    );

    // check global metrics
    // when counters have zero value, they are not exported
    assert!(find_metric(&scraped_lines, "alfred_num_system_errors").is_none());
    assert!(find_metric(&scraped_lines, "alfred_num_failed_jobs").is_none());
    assert_metric_eq(&scraped_lines, "alfred_num_launched_jobs", 5.);
    assert_metric_eq(&scraped_lines, "alfred_num_succeeded_jobs", 4.);
    assert_metric_eq(&scraped_lines, "alfred_num_register_errors", 0.);

    compose.down();
    // non-blocking job, should always have delta t = 3
    {
        let mut log_fname = compose.out_dir().path().to_path_buf();
        log_fname.push("log1.txt");
        let contents = parse_logfile(&log_fname);
        println!("contents: {:?}", contents);
        assert_eq!(contents.len(), 3);

        for i in 1..contents.len() - 1 {
            let start_delta = contents[i].timestamp - contents[i - 1].timestamp;
            assert_gt!(start_delta, 2.9);
            assert_lt!(start_delta, 3.1)
        }
    }

    // blocking job
    {
        let mut log_fname = compose.out_dir().path().to_path_buf();
        log_fname.push("log2.txt");
        let contents = parse_logfile(&log_fname);
        assert_eq!(contents.len(), 2);

        // start_delta = job 2 - job 1 start times
        let start_delta = contents[1].timestamp - contents[0].timestamp;
        assert_gt!(start_delta, 4.8);
        assert_lt!(start_delta, 5.2)
    }
}

#[test]
fn no_overlap_cron_job_skips_job_if_blocking_job_takes_too_long() {
    let compose_dir = make_compose_project_dir(
        "
- alfred.enabled=true
# two jobs: one finishes instantly, the other one sleeps a bit more than the schedule time
- alfred.job-exec.refjob.schedule=*/3 * * * * *
- alfred.job-exec.refjob.command=bash -c \"date +'%s.%N %M:%S' >> /out/log1.txt\"
- alfred.job-exec.refjob.no-overlap=true

# will start at t<=3 seconds after alfred starts up, sleep for 5 seconds.
# When the first job finishes, should wait one more second since it missed the previous instant
- alfred.job-exec.blockedjob.schedule=*/3 * * * * *
- alfred.job-exec.blockedjob.command=bash -c \"date +'%s.%N %M:%S' >> /out/log2.txt && sleep 5\"
- alfred.job-exec.blockedjob.no-overlap=true
",
        ComposeOpts::all_enabled(),
    );

    let compose = DockerComposeProject::new(
        "no_overlap_cron_job_skips_job_if_blocking_job_takes_too_long",
        compose_dir.path().to_path_buf(),
        DockerVars::new_empty(),
    );
    compose.up();
    assert!(compose.wait_for_alfred(Duration::from_secs(1)), "Alfred not responding");

    std::thread::sleep(Duration::from_secs(10));

    let scraped_lines = compose.scrape_or_panic();

    for job_name in ["refjob", "blockedjob"] {
        assert_metric_eq(
            &scraped_lines,
            format!(
                r#"alfred_job_num_failed_runs{{container="samplecontainer",task="{}"}}"#,
                job_name
            )
            .as_str(),
            0.,
        );
        assert_metric_eq(
            &scraped_lines,
            format!(r#"alfred_job_registered{{container="samplecontainer",task="{}"}}"#, job_name)
                .as_str(),
            1.,
        );
    }

    assert_metric_eq(
        &scraped_lines,
        r#"alfred_job_num_ok_runs{container="samplecontainer",task="blockedjob"}"#,
        1.,
    );

    assert_metric_ge(
        &scraped_lines,
        r#"alfred_job_num_ok_runs{container="samplecontainer",task="refjob"}"#,
        3.,
    );

    // check global metrics
    // when counters have zero value, they are not exported
    assert!(find_metric(&scraped_lines, "alfred_num_system_errors").is_none());
    assert!(find_metric(&scraped_lines, "alfred_num_failed_jobs").is_none());
    assert_metric_ge(&scraped_lines, "alfred_num_launched_jobs", 5.);
    assert_metric_ge(&scraped_lines, "alfred_num_succeeded_jobs", 4.);
    assert_metric_eq(&scraped_lines, "alfred_num_register_errors", 0.);

    compose.down();
    // non-blocking job, should always have delta t = 3
    {
        let mut log_fname = compose.out_dir().path().to_path_buf();
        log_fname.push("log1.txt");
        let contents = parse_logfile(&log_fname);
        assert_ge!(contents.len(), 3);

        for i in 1..contents.len() - 1 {
            let start_delta = contents[i].timestamp - contents[i - 1].timestamp;
            assert_gt!(start_delta, 2.9);
            assert_lt!(start_delta, 3.1)
        }
    }

    // blocking job
    {
        let mut log_fname = compose.out_dir().path().to_path_buf();
        log_fname.push("log2.txt");
        let contents = parse_logfile(&log_fname);
        assert_ge!(contents.len(), 2);

        // start time of first two should be 2 x 3 seconds
        let start_delta = contents[1].timestamp - contents[0].timestamp;
        assert_gt!(start_delta, 5.9);
        assert_lt!(start_delta, 6.1)
    }
}

#[rstest]
#[case::cron("cron", "*/2 * * * * *")]
#[case::every("every", "@every 2s")]
fn job_that_runs_forever_with_overlap_gets_spawned_many_times(
    #[case] test_type: &str, #[case] schedule: &str,
) {
    let compose_dir = make_compose_project_dir(
        format!(
            "
- alfred.enabled=true
- alfred.job-exec.foreverjob.schedule={schedule}
- alfred.job-exec.foreverjob.command=bash -c \"date +'%s %M:%S' >> /out/log.txt && sleep inf\"
- alfred.job-exec.foreverjob.no-overlap=false
"
        )
        .as_str(),
        ComposeOpts::all_enabled(),
    );

    let compose = DockerComposeProject::new(
        format!("job_runs_forever_spawned_many_times_{test_type}").as_str(),
        compose_dir.path().to_path_buf(),
        DockerVars::new_empty(),
    );
    compose.up();
    assert!(compose.wait_for_alfred(Duration::from_secs(1)), "Alfred not responding");

    std::thread::sleep(Duration::from_secs(10));

    let scraped_lines = compose.scrape_or_panic();

    assert_metric_eq(
        &scraped_lines,
        r#"alfred_job_registered{container="samplecontainer",task="foreverjob"}"#,
        1.,
    );

    // job 1 hasn't finished, so num ok runs == 0, and since it is a counter
    // it means it is not present
    assert!(find_metric(
        &scraped_lines,
        r#"alfred_job_num_ok_runs{container="samplecontainer",task="job1"}"#
    )
    .is_none());

    // check global metrics
    // when counters have zero value, they are not exported
    assert!(find_metric(&scraped_lines, "alfred_num_system_errors").is_none());
    assert!(find_metric(&scraped_lines, "alfred_num_failed_jobs").is_none());
    assert_metric_gt(&scraped_lines, "alfred_num_launched_jobs", 3.);
    assert!(find_metric(&scraped_lines, "alfred_num_succeeded_jobs").is_none());
    assert_metric_eq(&scraped_lines, "alfred_num_register_errors", 0.);

    compose.down();

    if test_type == "cron" {
        // read files and verify expected values
        for (job_name, interval_sec) in vec![("log.txt", 2)] {
            let mut log_fname = compose.out_dir().path().to_path_buf();
            log_fname.push(job_name);
            check_logfile(&parse_logfile(&log_fname), interval_sec);
        }
    }
}

#[rstest]
#[case::cron("cron", "*/1 * * * * *")]
#[case::every("every", "@every 1s")]
fn alfred_restarting_does_not_start_no_overlap_job_while_still_running(
    #[case] test_type: &str, #[case] schedule: &str,
) {
    let compose_dir = make_compose_project_dir(
        format!(
            "
- alfred.enabled=true
- alfred.job-exec.foreverjob.schedule={schedule}
- alfred.job-exec.foreverjob.command=bash -c \"date +'%s %M:%S' >> /out/log.txt && sleep 10s\"
- alfred.job-exec.foreverjob.no-overlap=true
"
        )
        .as_str(),
        ComposeOpts::all_enabled(),
    );

    let compose = DockerComposeProject::new(
        format!("alfred_restarting_does_not_start_no_overlap_job_while_running_{test_type}")
            .as_str(),
        compose_dir.path().to_path_buf(),
        DockerVars::new_empty(),
    );
    compose.up();
    assert!(compose.wait_for_alfred(Duration::from_secs(1)), "Alfred not responding");

    let mut log_fname = compose.out_dir().path().to_path_buf();
    log_fname.push("log.txt");

    let num_log_entries = || {
        if !log_fname.exists() {
            return 0;
        }
        let contents = parse_logfile(&log_fname);
        return contents.len();
    };

    // wait for job to start
    loop {
        let scraped_lines = compose.scrape_or_panic();

        if let Some(num_launched) = find_metric(&scraped_lines, "alfred_num_launched_jobs") {
            if num_launched == 1.0 && num_log_entries() == 1 {
                break;
            }
        }

        std::thread::sleep(Duration::from_millis(50));
    }

    assert_eq!(
        find_metric(&compose.scrape_or_panic(), "alfred_can_discover_existing_jobs").unwrap(),
        1.0
    );

    // stop alfred container and wait 2 seconds
    compose.service_down("alfred");
    std::thread::sleep(Duration::from_millis(2000));

    compose.service_up("alfred");
    assert!(compose.wait_for_alfred(Duration::from_secs(1)), "Alfred not responding");
    std::thread::sleep(Duration::from_millis(2000));

    // make sure we still haven't run again, metric should not be present
    {
        let scraped_lines = compose.scrape_or_panic();
        assert!(find_metric(&scraped_lines, "alfred_num_launched_jobs").is_none());
        assert_eq!(num_log_entries(), 1);
    }

    // if we wait long enough, should be able to run
    std::thread::sleep(Duration::from_millis(8000));
    {
        let scraped_lines = compose.scrape_or_panic();
        let num_launched = find_metric(&scraped_lines, "alfred_num_launched_jobs").unwrap();
        assert_eq!(num_launched, 1.0);
        assert_eq!(num_log_entries(), 2);
    }

    compose.down();
}

#[test]
fn alfred_cannot_discover_jobs_without_host_pid_or_ptrace() {
    for opts in [
        ComposeOpts::all_enabled().host_pid(false),
        ComposeOpts::all_enabled().sys_ptrace(false),
        ComposeOpts::all_enabled().sys_ptrace(false).host_pid(false),
    ]
    .into_iter()
    {
        let compose_dir = make_compose_project_dir(
                "
                    - alfred.enabled=true
                    - alfred.job-exec.foreverjob.schedule=*/1 * * * * *
                    - alfred.job-exec.foreverjob.command=bash -c \"date +'%s %M:%S' >> /out/log.txt && sleep 10s\"
                    - alfred.job-exec.foreverjob.no-overlap=true
                    ",
                opts.clone()
            );

        let compose = DockerComposeProject::new(
            "alfred_cannot_discover_jobs_without_host_pid_or_ptrace",
            compose_dir.path().to_path_buf(),
            DockerVars::new_empty(),
        );
        compose.up();
        assert!(compose.wait_for_alfred(Duration::from_secs(1)), "Alfred not responding");

        assert_eq!(
            find_metric(&compose.scrape_or_panic(), "alfred_can_discover_existing_jobs").unwrap(),
            0.0,
            "Unexpected metric value with opts = {:?}",
            opts
        );
    }
}

#[rstest]
#[case::cron("cron", "*/1 * * * * *")]
#[case::every("every", "@every 1s")]
fn job_that_runs_forever_with_no_overlap_spawned_once_when_docker_connectivity_fails_and_comes_back(
    #[case] test_type: &str, #[case] schedule: &str,
) {
    let compose_dir = make_compose_project_dir(
        format!(
            "
- alfred.enabled=true
- alfred.job-exec.foreverjob.schedule={schedule}
- alfred.job-exec.foreverjob.command=bash -c \"date +'%s %M:%S' >> /out/log.txt && sleep inf\"
- alfred.job-exec.foreverjob.no-overlap=true
"
        )
        .as_str(),
        ComposeOpts::all_enabled(),
    );

    let compose = DockerComposeProject::new(
        format!("job_that_runs_forever_with_no_overlap_spawned_once_when_docker_connectivity_fails_and_comes_back_{test_type}").as_str(),
        compose_dir.path().to_path_buf(),
        DockerVars::new_empty().alfred_docker_socket("tcp://localhost:1234").alfred_pre_delay("2s")
    );
    compose.up();

    // forward unix socket to tcp and stop after 4 seconds.
    // then sleep for 4 more seconds and come back.
    // This simulates losing connection,
    // and should recognize same job when socat is run again
    compose.docker(vec!["exec", "--detach", "alfred", "bash", "-c", 
        "timeout 4s socat TCP-LISTEN:1234,fork,reuseaddr UNIX-CONNECT:/var/run/docker.sock; sleep 4s; socat TCP-LISTEN:1234,fork,reuseaddr UNIX-CONNECT:/var/run/docker.sock"]);

    assert!(compose.wait_for_alfred(Duration::from_secs(3)), "Alfred not responding");

    std::thread::sleep(Duration::from_secs(10));

    let scraped_lines = compose.scrape_or_panic();

    assert_metric_eq(
        &scraped_lines,
        r#"alfred_job_registered{container="samplecontainer",task="foreverjob"}"#,
        1.,
    );

    // job hasn't finished
    assert_metric_eq(
        &scraped_lines,
        r#"alfred_job_num_ok_runs{container="samplecontainer",task="foreverjob"}"#,
        0.,
    );

    // at least once it will have failed beacuse of connectivity loss
    assert_metric_gt(
        &scraped_lines,
        r#"alfred_job_num_failed_runs{container="samplecontainer",task="foreverjob"}"#,
        0.,
    );

    // verify start time metric
    let log_start_time = {
        let mut log_fname = compose.out_dir().path().to_path_buf();
        log_fname.push("log.txt");
        let contents = parse_logfile(&log_fname);
        assert_eq!(contents.len(), 1, "Expected only one line in log file but got multiple");

        contents[0].timestamp
    };

    let metric_start_time = find_metric(
        &scraped_lines,
        r#"alfred_oldest_running_job_start_time{container="samplecontainer",task="foreverjob"}"#,
    )
    .expect("Could not get job start time metric");

    // launching the scheduler may take some time, give some leeway
    assert_gt!(metric_start_time, log_start_time - 0.5, "oldest job start metric out of bounds");
    assert_lt!(metric_start_time, log_start_time + 1.0, "oldest job start metric out of bounds");

    // check global metrics
    // when counters have zero value, they are not exported
    assert!(find_metric(&scraped_lines, "alfred_num_system_errors").is_none());
    assert_gt!(
        find_metric(&scraped_lines, "alfred_num_failed_jobs").expect("Cannot get num failed jobs"),
        0.0
    );
    assert_metric_eq(&scraped_lines, "alfred_num_launched_jobs", 1.);
    assert!(find_metric(&scraped_lines, "alfred_num_succeeded_jobs").is_none());
    assert_metric_eq(&scraped_lines, "alfred_num_register_errors", 0.);

    compose.down();

    // read log file to verify it was only run once
    {
        let mut log_fname = compose.out_dir().path().to_path_buf();
        log_fname.push("log.txt");
        let contents = parse_logfile(&log_fname);
        assert_eq!(contents.len(), 1, "Expected only one line in log file but got multiple");
    }
}
