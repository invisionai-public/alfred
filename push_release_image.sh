#!/bin/sh
set -e

REPO_DIR="$( cd "$( dirname "${BASH_SOURCE}" )" && pwd )"
VERSION=$1

if [ -z "$VERSION" ]; then
	echo "ERROR: version not specified"
	exit 1
fi

cd "$REPO_DIR"

REGISTRY=registry.gitlab.com/invisionai-public/alfred

# If buildx says there is no context or driver, try running before:
#    docker buildx create --driver docker-container --use
docker buildx build --platform=linux/arm64,linux/amd64 --push -t ${REGISTRY}:${VERSION} .
