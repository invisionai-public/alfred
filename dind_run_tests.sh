#!/bin/sh
# Runs tests with docker-in-docker
# Passes all arguments to cargo test
set -e

IMAGE_NAME=alfred-ci
REPO_DIR="$( cd "$( dirname "${BASH_SOURCE}" )" && pwd )"
CARGO_HOME_DIR="$REPO_DIR/artifacts/docker_cargo_home"

mkdir -p "$CARGO_HOME_DIR"

cd "$REPO_DIR"
docker build -f Dockerfile.ci -t $IMAGE_NAME .

# get docker group, if any
DOCKER_GID=$(getent group | grep docker | cut -d: -f3)
DOCKER_GID_ARG=
if [ ! -z "$DOCKER_GID" ]; then
	DOCKER_GID_ARG="--group-add=$DOCKER_GID"
fi

# network mode host to make it easy to access the container's prometheus exporter
# REPO_DIR must be mounted with the same destination mount point,
# as `target/` is used as a bind mount in docker tests
docker run --rm \
	--network host \
	-v /etc/passwd:/etc/passwd:ro \
	-v /etc/group:/etc/group:ro \
	-u $(id -u):$(id -g) \
	${DOCKER_GID_ARG} \
	-e CARGO_HOME=/.cargo \
	-v "$CARGO_HOME_DIR":/.cargo \
	-v /var/run/docker.sock:/var/run/docker.sock:ro \
	-v "$REPO_DIR":"$REPO_DIR" \
	-w "$REPO_DIR" \
	$IMAGE_NAME \
	cargo test "$@"
