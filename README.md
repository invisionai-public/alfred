# Alfred: a cron-like process scheduler for Docker
Alfred is a scheduler that runs jobs using Docker's API.

It is inspired by [Ofelia](https://github.com/mcuadros/ofelia/) and [Chadburn](https://github.com/PremoWeb/chadburn).

## Why develop a new software?
Ofelia [cannot handle restarting containers](https://github.com/mcuadros/ofelia/pull/137), which is a problem in some environments.
Chadburn fixes this problem but it needs a rewrite as tests are failing and [the project needs a rewrite](https://github.com/PremoWeb/chadburn/pull/71#issuecomment-2161600317).

Ofelia does not provide a Prometheus exporter, while Chadburn does, which is essential in some applications. Alfred provides it.

Alfred also handles container restarts, by detecting container changes and re-creating the schedules as needed.

## Using Alfred
Alfred is made to work with Docker, particularly Docker Compose.

For example:

```yaml
services:
  samplecontainer:
    image: bash
    init: true
    restart: always
    command: sleep inf
    labels:
      - alfred.enabled=true

      # will run every 10 seconds and at most have one job (no-overlap)
      - alfred.job-exec.job1.schedule=@every 10s
      - alfred.job-exec.job1.command=bash -c "date +'%s %M:%S' >> /tmp/job1.log"
      - alfred.job-exec.job1.no-overlap=true

      # will run at 23hs every day
      - alfred.job-exec.job2.schedule=0 0 23 ? * * *
      - alfred.job-exec.job2.command=bash -c "date +'%s %M:%S' >> /tmp/job2.log"

  alfred:
    image: alfred
    init: true  # to pass signals to main process, especially to pass sigterm
    command: --prometheus-port 9090
    ports:
      - 9090
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
    # pid host and SYS_PTRACE allow to detect running jobs if Alfred restarts
    # but are optional
    pid: host
    cap_add:
      - SYS_PTRACE
```

## Job specification
Docker labels are used to schedule jobs. The label `alfred.enabled` must be set to `true`, and jobs are added with

```yaml
labels:
  - alfred.job-exec.<job_name>.schedule=<SCHEDULE STRING>
  - alfred.job-exec.<job_name>.command=<COMMAND TO RUN>
  - alfred.job-exec.<job_name>.no-overlap=<bool>          # optional. defaults is false
  - alfred.job-exec.<job_name>.user=<username>            # optional. Otherwise container's default user
  - alfred.job-exec.<job_name>.enabled=<bool>             # optional. default is true
```

### Schedule String
The schedule string can be provided in two formats:

  1. Cron-like, including seconds resolution. See the [Rust cron package docs](https://docs.rs/cron/latest/cron/).
  2. As a period: syntax is `@every <duration>`. For example, `@every 10s`, `@every 20m 15s`


When specified cron-like, Alfred will strive to execute the command at the exact time given.
If `no-overlap = true` and a job is already running when a new scheduled job should run, the new job will be skipped.
This is on purpose, to follow as close as possible the cron rule given.

When specified as a period, it will be scheduled relative to the last execution or Alfred start time.
If `no-overlap = true` and a job is already running when a new scheduled job should run, the new job will be started
as soon as the previous one finishes.


## Discovering already-running Alfred jobs when Alfred restarts
Alfred can detect already-running jobs if it restarts. This common when docker-compose
restarts only certain containers. This matters because:

  - we want to keep process launch/finished/status metrics with their correct value
  - for non-overlap jobs, to avoid re-launching


**IMPORTANT**: discovered jobs will not output to stdout or stderr. This is a limitation with docker's API,
not being able to attach to an existing exec instance.

We use a mix of docker API, environment variables ready with SYS_PTRACE, and host pid namespaces.
This requires extra permissions: the container running Alfred must have the `SYS_PTRACE` cap and run in PID host namespace.

If any of these two permissions are unavailable, Alfred will still run but this functionality will be unavailable.

## Why 'Alfred'?
Alfred is Batman's buttler, helping him and, in this case, launching processes on schedule.
