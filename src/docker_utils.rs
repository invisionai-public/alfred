use crate::exec_job::ExecJob;
use crate::schedule::Schedule;
use anyhow::Context;
use clap::Parser;
use docker_api::Docker;
use log::warn;
use std::collections::HashMap;
use std::str::FromStr;
use uuid::Uuid;

static ALFRED_JOB_EXEC_LABEL: &str = "alfred.job-exec.";
static ALFRED_ENABLED_LABEL: &str = "alfred.enabled";

/// To specify labels to use to find project and service name
/// from compose labels
#[derive(Parser, Debug)]
pub struct DockerLabelsInfo {
    #[arg(
        long = "compose-project-name-label",
        help = "Container label storing compose project name. For balena use 'io.balena.app-id'",
        default_value = "com.docker.compose.project"
    )]
    pub compose_project_name: String,

    #[arg(
        long = "compose-service-name-label",
        help = "Container label storing service name. For belena use 'io.balena.service-name'",
        default_value = "com.docker.compose.service"
    )]
    pub compose_service_name: String,
}

// Prometheus metrics
use lazy_static::lazy_static;
use prometheus::{register_int_gauge, IntGauge};

lazy_static! {
    // Metrics common to all jobs
    static ref NUM_REGISTER_ERRORS: IntGauge =
        register_int_gauge!("alfred_num_register_errors", "Number of errors when trying to register jobs").unwrap();
}

pub struct JobsWithParseStats {
    jobs: Vec<ExecJob>,
    num_errors: u32,
}

impl JobsWithParseStats {
    fn new_empty() -> JobsWithParseStats {
        JobsWithParseStats { jobs: vec![], num_errors: 0 }
    }
}

fn string_to_bool(s: &str) -> Option<bool> {
    let lower = s.to_lowercase();
    if lower == "1" || lower == "true" {
        Some(true)
    } else if lower == "0" || lower == "false" {
        Some(false)
    } else {
        None
    }
}

/// Parse ExecJob's from docker labels
///  - label 'alfred.enabled' must be 'true' or 1
///  - each job declared with
///      - alfred.job-exec.<job_name>.command=echo hello
///      - alfred.job-exec.<job_name>.schedule=<cron-style schedule string>
///      - alfred.job-exec.<job_name>.user=<optional user for exec>
async fn container_labels_to_jobs(
    docker: Docker, container_id: &str, labels_info: &DockerLabelsInfo,
) -> anyhow::Result<JobsWithParseStats> {
    let inspect_data = docker.containers().get(container_id).inspect().await?;
    if inspect_data.config.is_none() {
        return Ok(JobsWithParseStats::new_empty());
    }

    let container_name = inspect_data.name.context("Cannot get container name")?;

    // find compose service name, if any
    let compose_service_name = match &inspect_data.config.as_ref().unwrap().labels {
        None => String::new(),
        Some(labels) => match labels.get(&labels_info.compose_service_name) {
            Some(value) => value.to_string(),
            None => String::new(),
        },
    };

    // group labels by job name to easy parsing later
    let mut job_name_labels: HashMap<&str, HashMap<&str, &str>> = HashMap::new();
    let mut num_errors: u32 = 0;

    match inspect_data.config.unwrap().labels {
        None => return Ok(JobsWithParseStats::new_empty()),
        Some(labels) => {
            // if not enabled, nothing to parse
            match labels.get(ALFRED_ENABLED_LABEL) {
                Some(value) => {
                    if *value != "1" && value.to_lowercase() != "true" {
                        return Ok(JobsWithParseStats::new_empty());
                    }
                }
                None => return Ok(JobsWithParseStats::new_empty()),
            }

            for (key, value) in labels.iter() {
                if key.starts_with(ALFRED_JOB_EXEC_LABEL) {
                    let tokens: Vec<&str> = key.split('.').into_iter().collect();
                    if tokens.len() != 4 {
                        warn!("Ignoring malformed label {}={}", key, value);
                        num_errors += 1;
                        continue;
                    }

                    match job_name_labels.get_mut(tokens[2]) {
                        Some(keys) => {
                            keys.insert(tokens[3], value.as_str());
                        }
                        None => {
                            job_name_labels
                                .insert(tokens[2], HashMap::from([(tokens[3], value.as_str())]));
                        }
                    }
                }
            }

            // Now parse per job
            let mut jobs: Vec<ExecJob> = vec![];
            for (ref job_name, ref key_value) in job_name_labels.iter() {
                if !key_value.contains_key("command") {
                    warn!("Job {} with no command. Ignoring.", job_name);
                    num_errors += 1;
                    continue;
                }

                let command_string = key_value.get("command").unwrap();
                let command = match shlex::split(command_string) {
                    Some(command) => command,
                    None => {
                        warn!(
                            "Could not parse commandline '{}' for job {}. Ignoring.",
                            command_string, job_name
                        );
                        num_errors += 1;
                        continue;
                    }
                };

                if !key_value.contains_key("schedule") {
                    warn!("Job {} with no schedule. Ignoring.", job_name);
                    num_errors += 1;
                    continue;
                }

                let no_overlap: bool = match key_value.get("no-overlap") {
                    None => false,
                    Some(value) => match string_to_bool(value) {
                        Some(no_overlap) => no_overlap,
                        None => {
                            warn!(
                                "Error parsing no-overlap to boolean for job {}. Got {}",
                                job_name, value
                            );
                            num_errors += 1;
                            continue;
                        }
                    },
                };

                let schedule = match Schedule::from_str(key_value.get("schedule").unwrap()) {
                    Ok(schedule) => schedule,
                    Err(err) => {
                        warn!("Error parsing schedule for job {}: {}", job_name, err);
                        num_errors += 1;
                        continue;
                    }
                };

                let user: Option<String> = match key_value.get("user") {
                    None => None,
                    Some(user) => Some(user.to_string()),
                };

                match key_value.get("enabled") {
                    None => (),
                    Some(value) => match string_to_bool(value) {
                        None => {
                            warn!(
                                "Error parsing schedule for job {}: enabled has invalid value '{}'",
                                job_name, value
                            );
                            num_errors += 1;
                            continue;
                        }
                        Some(enabled) => {
                            if !enabled {
                                continue;
                            }
                        }
                    },
                };

                jobs.push(ExecJob::new(
                    job_name.to_string(),
                    compose_service_name.clone(),
                    container_name.clone(),
                    container_id.to_string(),
                    command,
                    schedule,
                    user,
                    no_overlap,
                ));
            }
            return Ok(JobsWithParseStats { jobs, num_errors });
        }
    }
}

/// Returns the inspect information for a container with hostname `query_hostname`, if found.
/// If no container matches this hostname, returns None
pub async fn inspect_container_with_hostname(
    docker: Docker, query_hostname: &str,
) -> Result<Option<docker_api::models::ContainerInspect200Response>, docker_api::Error> {
    for container in docker
        .containers()
        .list(&docker_api::opts::ContainerListOpts::builder().all(true).build())
        .await?
        .into_iter()
    {
        if container.id.is_none() {
            continue;
        } // dead containers may have no id
        let container_id = container.id.unwrap();

        let inspect = docker.containers().get(container_id.clone()).inspect().await?;

        match &inspect.state {
            None => continue,
            Some(state) => match state.running {
                None => continue,
                Some(running) => {
                    if !running {
                        continue;
                    }
                }
            },
        }

        let config = inspect.config.as_ref().unwrap();
        let container_hostname = config.hostname.as_ref().expect("Cannot get container hostname");
        if container_hostname == query_hostname {
            return Ok(Some(inspect));
        }
    }

    return Ok(None); // no container with given hostname
}

/// Returns, if present, the value for label `label_name` for a running container with hostname
/// `query_hostname`.
/// If no container is found with such host name or `label_name` is not present, it returns Optional::None
pub async fn label_from_container_with_hostname(
    docker: Docker, query_hostname: &str, label_name: &str,
) -> anyhow::Result<Option<String>> {
    let inspect = inspect_container_with_hostname(docker, query_hostname).await?;

    match inspect {
        None => return Ok(None),

        Some(inspect) => {
            let config = inspect.config.context("Could not get inspect.config")?;
            let mut labels = match config.labels {
                None => return Ok(None),
                Some(labels) => labels,
            };

            match labels.remove(label_name) {
                None => return Ok(None),
                Some(label_value) => return Ok(Some(label_value)),
            }
        }
    };
}

/// Parse periodic jobs defined as labels of containers.
/// If `docker_compose_project` is None, all containers are scanned,
/// If Some(), then only those that are in that compose project are scanned
///   `project_name_label` is the key of the label that indicates a container
///   belongs to a given compose project, with project_name_label=project_name
pub async fn periodic_jobs_from_docker_labels(
    docker: Docker, compose_project_name: &Option<String>, labels_info: &DockerLabelsInfo,
) -> anyhow::Result<HashMap<Uuid, ExecJob>> {
    let mut num_total_parse_errors: u32 = 0;

    // get jobs from all containers
    let mut uuid_to_job: HashMap<Uuid, ExecJob> = HashMap::new();
    for container in docker.containers().list(&Default::default()).await?.into_iter() {
        let container_id = container.id.as_ref().context("Could not get container id")?.clone();

        let container_name = match &container.names {
            Some(names) => {
                if names.len() == 0 {
                    warn!(
                        "Could not get container name for container id {}. Ignoring.",
                        container_id
                    );
                    continue;
                }
                names[0].clone()
            }
            None => {
                warn!("Could not get container name for container id {}. Ignoring.", container_id);
                continue;
            }
        };

        let labels = match container.labels {
            Some(labels) => labels,
            None => HashMap::new(),
        };

        if compose_project_name.is_some() {
            match labels.get(&labels_info.compose_project_name) {
                Some(container_compose_project) => {
                    if *container_compose_project != *compose_project_name.as_ref().unwrap() {
                        continue;
                    }
                }
                None => continue,
            }
        }
        match container_labels_to_jobs(
            docker.clone(),
            &container.id.as_ref().unwrap(),
            &labels_info,
        )
        .await
        {
            Ok(jobs_with_parse_errors) => {
                for job in jobs_with_parse_errors.jobs.into_iter() {
                    uuid_to_job.insert(job.job.uuid, job);
                }

                num_total_parse_errors += jobs_with_parse_errors.num_errors;
            }
            Err(err) => {
                warn!("Could not get job labels for container {}: {}", container_name, err);
                continue;
            }
        };
    }

    NUM_REGISTER_ERRORS.set(num_total_parse_errors as i64);

    Ok(uuid_to_job)
}
