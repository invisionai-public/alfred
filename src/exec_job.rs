use std::time::SystemTime;
use std::time::UNIX_EPOCH;

use docker_api::{conn::TtyChunk, Docker};
use futures::StreamExt;
use log::{info, warn};
use tokio::time::timeout;
use tokio::time::Duration;

use crate::exec_discover::ALFRED_MANAGED_CONTAINER_VARNAME;
use crate::exec_discover::ALFRED_MANAGED_JOB_VARNAME;
use crate::exec_discover::ALFRED_MANAGED_START_TIMESTAMP_VARNAME;
use crate::job::{Job, JobTrait};
use crate::schedule::Schedule;

// Information to schedule a job periodically
// on a given container
#[derive(Clone)]
pub struct ExecJob {
    pub job: Job,
    pub schedule: Schedule,
}

impl JobTrait for ExecJob {
    fn job(&self) -> &Job {
        &self.job
    }
}

impl ToString for ExecJob {
    fn to_string(&self) -> String {
        self.job.to_string()
    }
}

impl ExecJob {
    pub fn new(
        name: String, compose_service_name: String, container_name: String, container_id: String,
        command: Vec<String>, schedule: Schedule, user: Option<String>, no_overlap: bool,
    ) -> Self {
        Self {
            job: Job::new(
                name,
                compose_service_name,
                container_name,
                container_id,
                command,
                user,
                no_overlap,
            ),
            schedule,
        }
    }

    // Launch an exec instance, to call `launch_and_wait_finish`.
    // Handy to get hold of the exec id of the job before it starts
    pub async fn prepare_docker_exec(
        &self, docker: Docker,
    ) -> docker_api::Result<docker_api::Exec> {
        use docker_api::opts::ExecCreateOpts;
        let start_timestamp = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_millis();

        let mut options_builder = ExecCreateOpts::builder()
            .command(self.command().clone())
            .attach_stdout(true)
            .attach_stderr(true)
            .tty(false)
            .env(&[
                format!(
                    "{}={}",
                    ALFRED_MANAGED_CONTAINER_VARNAME,
                    self.service_else_container_name()
                ),
                format!("{}={}", ALFRED_MANAGED_JOB_VARNAME, self.name()),
                format!("{}={}", ALFRED_MANAGED_START_TIMESTAMP_VARNAME, start_timestamp),
            ]);

        match &self.user() {
            Some(username) => options_builder = options_builder.user(username.clone()),
            None => (),
        }

        let options = options_builder.build();

        return docker_api::Exec::create(docker.clone(), self.container_id().clone(), &options)
            .await;
    }

    // Start exec and return exit code when execution finishes.
    // Forwards stdin, stdout and stderr of job to log info
    pub async fn start_and_wait_finish(&self, exec: docker_api::Exec) -> docker_api::Result<isize> {
        let job_str = self.job.to_string();

        let mut stream = exec.start(&Default::default()).await?;

        loop {
            match timeout(Duration::from_secs(60), stream.next()).await {
                Ok(Some(Ok(chunk))) => match chunk {
                    TtyChunk::StdOut(buf) => {
                        let msg = std::str::from_utf8(&buf).unwrap_or("<Error: not a utf8 string>");
                        for line in msg.lines() {
                            info!(target: &job_str, "[stdout] {}", line);
                        }
                    }
                    TtyChunk::StdErr(buf) => {
                        let msg = std::str::from_utf8(&buf).unwrap_or("<Error: not a utf8 string>");
                        for line in msg.lines() {
                            info!(target: &job_str, "[stderr] {}", line);
                        }
                    }
                    // this won't happen, but we need to have it for completeness or rust won't
                    // compile
                    TtyChunk::StdIn(buf) => {
                        warn!(target: &job_str,"Unexpected STDIN value: {}", std::str::from_utf8(&buf).unwrap_or("<Error: not a utf8 string>"));
                    }
                },
                // timeout, to check it's still running in case something happened
                Err(_) => {
                    match exec.inspect().await?.running {
                        None => {
                            warn!("After timing out while waiting for job: no running info for job {}", job_str);
                            break;
                        } // something's off, we expect this var to be there
                        Some(running) => {
                            if !running {
                                warn!("Detected job done through timeout for {}", job_str);
                                break;
                            }
                        }
                    }
                }
                // nothing else to poll, stream closed
                _ => break,
            }
        }

        if let Some(exit_code) = exec.inspect().await?.exit_code {
            return Ok(exit_code);
        } else {
            return Err(docker_api::Error::StringError("Could not get exit code".to_string()));
        }
    }
}
