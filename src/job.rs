use uuid::Uuid;

/// All types of jobs contain these fields
#[derive(Clone)]
pub struct Job {
    pub name: String,
    pub compose_service_name: String, // or empty if not running under compose
    pub container_name: String,
    pub container_id: String,
    pub command: Vec<String>,
    pub user: Option<String>,
    pub no_overlap: bool,
    pub uuid: Uuid,
}

impl Job {
    pub fn new(
        name: String, compose_service_name: String, container_name: String, container_id: String,
        command: Vec<String>, user: Option<String>, no_overlap: bool,
    ) -> Self {
        let mut data: Vec<u8> = vec![];
        data.extend_from_slice(name.as_bytes());
        data.extend_from_slice(container_id.as_bytes());

        let uuid = Uuid::new_v5(&Uuid::NAMESPACE_DNS, &data.as_slice());
        Self {
            name,
            compose_service_name,
            container_name,
            container_id,
            command,
            user,
            no_overlap,
            uuid,
        }
    }

    // Returns service name if available, otherwise the  container name.
    // Handy because container names in docker compose can be long,
    // so service names are shorter. But if we don't run in docker compose,
    // then no service name available
    pub fn service_else_container_name(&self) -> &String {
        if !self.compose_service_name.is_empty() {
            &self.compose_service_name
        } else {
            &self.container_name
        }
    }

    // Handy to show a string representation of the job
    pub fn to_string(&self) -> String {
        format!("{}.{}", self.service_else_container_name(), self.name)
    }
}

/// When creating a new job, implement this trait
/// and define fn job(&self) -> &Job
pub trait JobTrait {
    fn job(&self) -> &Job;

    fn name(&self) -> &String {
        &self.job().name
    }

    // Returns service name if available, otherwise the  container name.
    // Handy because container names in docker compose can be long,
    // so service names are shorter. But if we don't run in docker compose,
    // then no service name available
    fn service_else_container_name(&self) -> &String {
        self.job().service_else_container_name()
    }

    fn container_id(&self) -> &String {
        &self.job().container_id
    }
    fn container_name(&self) -> &String {
        &self.job().container_id
    }

    fn uuid(&self) -> Uuid {
        self.job().uuid
    }

    fn command(&self) -> &Vec<String> {
        &self.job().command
    }

    fn user(&self) -> &Option<String> {
        &self.job().user
    }
}

impl ToString for dyn JobTrait {
    fn to_string(&self) -> String {
        self.job().to_string()
    }
}

impl JobTrait for Job {
    fn job(&self) -> &Job {
        &self
    }
}
