use std::mem::swap;
use std::time::{SystemTime, UNIX_EPOCH};

use chrono::Local;
use docker_api::Docker;
use futures::TryFutureExt;
use log::{debug, error, info, warn};
use prometheus::core::{AtomicU64, GenericCounter};
use tokio::time::Duration;

use crate::exec_discover::{discover_alfred_managed_processes, DiscoveredProcess};
use crate::exec_job::ExecJob;

use crate::future_utils::{
    get_oldest_start_time_or_zero, select_with_timeout, FutureType, FutureWithStartTime,
};

// Prometheus metrics
use lazy_static::lazy_static;
use prometheus::{
    register_gauge_vec, register_int_counter, register_int_counter_vec, register_int_gauge_vec,
    GaugeVec,
};
use prometheus::{IntCounter, IntCounterVec, IntGaugeVec};

lazy_static! {
    // Metrics common to all jobs
    static ref NUM_FAILED_JOBS: IntCounter =
        register_int_counter!("alfred_num_failed_jobs", "Number of jobs that failed").unwrap();
    static ref NUM_SYSTEM_ERRORS: IntCounter =
        register_int_counter!("alfred_num_system_errors", "Number of system errors").unwrap();
    static ref NUM_LAUNCHED_JOBS: IntCounter =
        register_int_counter!("alfred_num_launched_jobs", "Number of jobs launched").unwrap();
    static ref NUM_SUCCEEDED_JOBS: IntCounter =
        register_int_counter!("alfred_num_succeeded_jobs", "Number of jobs with return value 0")
            .unwrap();

    // Per-job metrics, using labels
    // each job now registered will have a '1' value here
    // We use `task` instead of `job` for the label name, because `job` is used as the scrape job
    // name in prometheus
    static ref REGISTERED_JOB: IntGaugeVec =
        register_int_gauge_vec!("alfred_job_registered", "Registered jobs", &["task", "container"]).unwrap();
    static ref JOB_NUM_SUCCEEDED_RUNS: IntCounterVec =
        register_int_counter_vec!("alfred_job_num_ok_runs", "Succeeded runs", &["task", "container"]).unwrap();
    static ref JOB_NUM_FAILED_RUNS: IntCounterVec =
        register_int_counter_vec!("alfred_job_num_failed_runs", "Failed runs", &["task", "container"]).unwrap();

    // 0 if job not running, timestamp in milliseconds if running
    static ref OLDEST_RUNNING_JOB_START_TIME: GaugeVec = register_gauge_vec!("alfred_oldest_running_job_start_time", "Oldest job start time or zero if none", &["task", "container"]).unwrap();
}

/// Execute a lambda when dropped
struct FuncOnDrop<'a> {
    drop_fn: Box<dyn Fn() + 'a + Send>,
}

impl<'a> FuncOnDrop<'a> {
    fn new(drop_fn: Box<dyn Fn() + 'a + Send>) -> Box<FuncOnDrop> {
        Box::new(FuncOnDrop { drop_fn })
    }
}

impl<'a> Drop for FuncOnDrop<'a> {
    fn drop(&mut self) {
        (&self.drop_fn)();
    }
}

// Duration cannot store negative values, so we return None
// if `dt` is in the past
fn time_until(dt: &chrono::DateTime<Local>) -> Option<Duration> {
    let now_duration = std::time::SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Could not get since epoch time");

    let dt_duration = Duration::from_micros(dt.timestamp_micros().try_into().unwrap());

    dt_duration.checked_sub(now_duration)
}

async fn start_and_wait_finish(
    job: ExecJob, exec: docker_api::Exec, num_ok_runs_metric: GenericCounter<AtomicU64>,
    num_failed_runs_metric: GenericCounter<AtomicU64>,
) {
    let job_str = job.to_string();

    NUM_LAUNCHED_JOBS.inc();
    let launch_time = SystemTime::now();

    let mut success: bool = false;
    match job.start_and_wait_finish(exec).await {
        Ok(ret_code) => {
            success = ret_code == 0;
            info!(target: &job_str, "Exited with code {}. Took {:.3} seconds.", ret_code, SystemTime::now().duration_since(launch_time).unwrap().as_secs_f64());
        }
        Err(err) => error!(target: &job_str, "Error launching job: {:?}", err),
    }

    if success {
        num_ok_runs_metric.inc();
        NUM_SUCCEEDED_JOBS.inc();
    } else {
        num_failed_runs_metric.inc();
        NUM_FAILED_JOBS.inc();
    }
}

/// Used when exec was already running due to an Alfred restart
/// We cannot get the process' stdout/err, so we can just wait until it finishes
async fn wait_exec_to_finish(
    job: ExecJob, docker: Docker, exec_id: String, num_ok_runs_metric: GenericCounter<AtomicU64>,
    num_failed_runs_metric: GenericCounter<AtomicU64>,
) {
    let job_str = job.to_string();

    // resort to polling, as we don't have any async handle to know when it's done
    let mut success: bool = false;
    let exec = docker_api::Exec::get(docker, &exec_id);
    loop {
        match exec.inspect().await {
            Err(err) => {
                warn!(target: &job_str, "Could not read exec status: {}. Assuming it finished.", err);
                break;
            }
            Ok(inspect) => {
                if let Some(exit_code) = inspect.exit_code {
                    success = exit_code == 0;
                    info!(target: &job_str, "Exited with code {}", exit_code);
                    break;
                }
            }
        }

        tokio::time::sleep(Duration::from_millis(500)).await;
    }

    if success {
        num_ok_runs_metric.inc();
        NUM_SUCCEEDED_JOBS.inc();
    } else {
        num_failed_runs_metric.inc();
        NUM_FAILED_JOBS.inc();
    }
}

/// Return exec_ids of exec instances for `job` already running.
/// Handy to manage Alfred restarts
async fn execs_already_running(
    docker: Docker, exec_job: &ExecJob, ignore_exec_ids: &Vec<&String>,
) -> docker_api::Result<Vec<DiscoveredProcess>> {
    let discovered_processes =
        discover_alfred_managed_processes(docker.clone(), &exec_job.job.container_id).await?;

    return Ok(discovered_processes
        .into_iter()
        .filter(|p| {
            if !p.matches_job(&exec_job) {
                return false;
            }
            let matches_ignore_ids = ignore_exec_ids.iter().any(|e| **e == p.exec_id);
            return !matches_ignore_ids;
        })
        .collect());
}

async fn existing_exec_wait_futures(
    docker: &Docker, job: &ExecJob, ignore_exec_ids: &Vec<&String>,
    num_ok_runs_metric: &GenericCounter<AtomicU64>,
    num_failed_runs_metric: &GenericCounter<AtomicU64>,
) -> docker_api::Result<Vec<FutureWithStartTime>> {
    let already_running = execs_already_running(docker.clone(), &job, ignore_exec_ids).await?;
    if already_running.is_empty() {
        return Ok(vec![]);
    }

    let mut futures = vec![];
    for discovered_proc in already_running.into_iter() {
        let exec_id = discovered_proc.exec_id;

        let task_handle = tokio::spawn(wait_exec_to_finish(
            job.clone(),
            docker.clone(),
            exec_id.clone(),
            num_ok_runs_metric.clone(),
            num_failed_runs_metric.clone(),
        ));

        futures.push(FutureWithStartTime::new(
            Box::new(
                // make return value be ()
                task_handle.map_ok_or_else(|_| {}, |_| {}),
            ),
            FutureType::Task(exec_id),
            discovered_proc.start_time,
        ))
    }

    Ok(futures)
}

/// Schedule and run `job` periodically forever
pub async fn run_scheduled(exec_job: ExecJob, docker: Docker) {
    let job_str = exec_job.to_string();
    let job = &exec_job.job;

    let metric_labels = &[job.name.as_str(), job.service_else_container_name()];
    let job_registered_metric = REGISTERED_JOB.with_label_values(metric_labels);
    job_registered_metric.set(1);
    let _cleanup = FuncOnDrop::new(Box::new(move || {
        job_registered_metric.set(0);
    }));
    let oldest_start_time_metric = OLDEST_RUNNING_JOB_START_TIME.with_label_values(metric_labels);
    oldest_start_time_metric.set(0.0);

    let num_ok_runs_metric = JOB_NUM_SUCCEEDED_RUNS.with_label_values(metric_labels);
    let num_failed_runs_metric = JOB_NUM_FAILED_RUNS.with_label_values(metric_labels);

    // always sorted ascending by start time
    // Stored in two different vecs because select_all() requires a Vec<Future> anyway
    let mut launched_tasks: Vec<FutureWithStartTime> = vec![];

    let mut last_loop_docker_error = false;

    // For @every jobs, we assume we just launched one, so we'll wait
    // for next time
    let mut last_launch_time: SystemTime = SystemTime::now();

    loop {
        // if we saw an error, we don't want to retry too soon
        if last_loop_docker_error {
            last_loop_docker_error = false;
            tokio::time::sleep(Duration::from_millis(1000)).await;
        }

        // detect existing jobs, in case Alfred restarted, or the docker daemon
        // stopped responding and we couldn't wait for a job to finish
        {
            let mut existing_ids = vec![];
            for task in launched_tasks.iter() {
                match task.future_type() {
                    FutureType::Task(exec_id) => existing_ids.push(exec_id),
                    FutureType::Timeout => (),
                }
            }

            let existing_wait_futures = match existing_exec_wait_futures(
                &docker,
                &exec_job,
                &existing_ids,
                &num_ok_runs_metric,
                &num_failed_runs_metric,
            )
            .await
            {
                Ok(result) => result,
                Err(error) => {
                    warn!(target:&job_str, "Docker error getting existing wait futures: {:?}. Will retry.", error);
                    last_loop_docker_error = true;
                    continue;
                }
            };

            if !existing_wait_futures.is_empty() {
                warn!(target: &job_str, "Alfred likely to have restarted: Found {} jobs already executing", existing_wait_futures.len());
                launched_tasks.extend(existing_wait_futures.into_iter());
            }
        }

        // Wait for timeout or launched tasks to finish,
        // to update oldest start time and break out if we're ready for a new
        // task to be launched
        loop {
            oldest_start_time_metric.set(get_oldest_start_time_or_zero(&launched_tasks));

            let next_launch_time =
                match exec_job.schedule.upcoming_next(&last_launch_time, &SystemTime::now()) {
                    Some(next_time) => next_time,
                    None => {
                        info!(target: &job_str, "No next scheduled time. Stopping scheduler");
                        // TODO: unlikely to be here, but wait until tasks finish and update metrics
                        return;
                    }
                };
            debug!(target: &job_str, "Next run at {}", next_launch_time);

            let wait_time = match time_until(&next_launch_time) {
                Some(sleep_time) => sleep_time,

                // time is in the past, no need to sleep
                None => Duration::from_secs(0),
            };

            // If we passed the deadline, no need for timeout
            let maybe_deadline: Option<tokio::time::Instant> = {
                let now = std::time::Instant::now();
                let next_deadline = now + wait_time;

                if next_deadline < now {
                    None
                } else {
                    Some(next_deadline.into())
                }
            };

            // Wait for task or timeout
            let mut tasks_to_await = vec![];
            swap(&mut launched_tasks, &mut tasks_to_await);

            let timedout: bool;
            (launched_tasks, timedout) = select_with_timeout(tasks_to_await, maybe_deadline).await;

            oldest_start_time_metric.set(get_oldest_start_time_or_zero(&launched_tasks));

            // need to start new job if
            // 1. time out for next job happened, or
            // 2. there was no timeout, but no more running tasks
            if timedout || maybe_deadline.is_none() {
                if job.no_overlap == false || launched_tasks.is_empty() {
                    break;
                }
            }
        }

        // if command is too long, truncate it
        let max_display_len = 60;
        let mut joined_name = job.command.iter().as_ref().join(",");
        if joined_name.len() > max_display_len {
            joined_name.truncate(max_display_len - 3);
            joined_name += "...";
        }
        info!(target: &job_str, "Launching [{}]", joined_name);

        let docker_exec = match exec_job.prepare_docker_exec(docker.clone()).await {
            Ok(exec) => exec,
            Err(error) => {
                warn!(target:&job_str, "Docker error preparing exec: {:?}", error);
                last_loop_docker_error = true;
                continue;
            }
        };

        let exec_id = docker_exec.id().clone();

        let task_handle = tokio::spawn(start_and_wait_finish(
            exec_job.clone(),
            docker_exec,
            num_ok_runs_metric.clone(),
            num_failed_runs_metric.clone(),
        ));
        last_launch_time = SystemTime::now();

        launched_tasks.push(FutureWithStartTime::new(
            Box::new(
                // make return value be ()
                task_handle.map_ok_or_else(|_| {}, |_| {}),
            ),
            FutureType::Task(exec_id.as_ref().to_string()),
            SystemTime::now(),
        ));
    }
}
