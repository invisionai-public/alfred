// Utils to discover exec processes in a container managed by Alfred
// after Alfred has restarted.
// We need to inspect the env vars of each exec process,
// so we need CAP_SYS_PTRACE and to be in the host's PID namespace.

use crate::docker_utils::inspect_container_with_hostname;
use crate::exec_job::ExecJob;
use anyhow::Context;
use caps::{CapSet, Capability};
use chrono::{DateTime, Utc};
use docker_api::Docker;
use log::{debug, trace, warn};
use std::ffi::{OsStr, OsString};
use std::fs::File;
use std::os::unix::ffi::OsStrExt;
use std::time::SystemTime;
use std::{collections::HashMap, io::Read};

// if a process is managed by alfred it'll have this env vars set
// to container/service name and job name
pub static ALFRED_MANAGED_CONTAINER_VARNAME: &str = &"__ALFRED_MANAGED_CONTAINER";
pub static ALFRED_MANAGED_JOB_VARNAME: &str = &"__ALFRED_MANAGED_JOB";

// We could read this from /proc/PID/stat, but it's easier from here atm.
// If we read it from /proc/PID/stat, we need special permissions we may not have
// if re-discovery is not needed.
pub static ALFRED_MANAGED_START_TIMESTAMP_VARNAME: &str = &"__ALFRED_MANAGED_START_TIME";

/// True if this process has CAP_SYS_PTRACE access,
/// needed to read /proc/PID/environ
fn has_cap_sys_ptrace() -> bool {
    match caps::has_cap(None, CapSet::Permitted, Capability::CAP_SYS_PTRACE) {
        Err(err) => {
            warn!("Could not check CAP_SYS_PTRACE_SUPPORT: '{}' Assuming not available.", err);
            return false;
        }
        Ok(has_cap) => {
            if !has_cap {
                warn!("CAP_SYS_PTRACE not alllowed");
                return false;
            } else {
                return true;
            }
        }
    };
}

/// Returns true if this process is not running inside docker (meaning it runs on the host),
/// or if the docker container has PidMode = host
async fn process_in_pid_host_namespace(docker: Docker) -> bool {
    // find out if we run inside a docker container and the docker compose project name
    let hostname = hostname::get().expect("Could not get hostname");
    let inspect = inspect_container_with_hostname(docker, hostname.to_str().unwrap()).await;
    match inspect {
        Err(err) => {
            warn!("Error inspecting container: {}", err);
            return false;
        }
        Ok(inspect) => match inspect {
            None => {
                warn!("Could not find container with hostname, assuming running on host namespace");
                return true;
            }
            Some(inspect) => {
                let json = inspect.host_config.expect("Could not get host config");
                let json_obj = json.as_object().expect("Could not get host config object");
                return json_obj
                    .get("PidMode")
                    .expect("Could not get PidMode key")
                    .as_str()
                    .unwrap_or_default()
                    == "host";
            }
        },
    };
}

pub async fn can_discover_existing_jobs(docker: Docker) -> bool {
    return has_cap_sys_ptrace() && process_in_pid_host_namespace(docker).await;
}

pub fn get_pid_env_vars(pid: i32) -> anyhow::Result<HashMap<OsString, OsString>> {
    let env_file = format!("/proc/{}/environ", pid);
    let mut file =
        File::open(&env_file).with_context(|| format!("Could not open file {}", env_file))?;

    let mut bytes: Vec<u8> = vec![];
    file.read_to_end(&mut bytes)
        .with_context(|| format!("Error reading {}", env_file))?;

    let mut env_var_map = HashMap::<OsString, OsString>::new();

    for entry in bytes.split(|b| *b == 0) {
        // entry in key=value format.
        // Split on first equal sign
        let mut kvsplit = entry.splitn(2, |b| *b == b'=');
        if let (Some(k), Some(v)) = (kvsplit.next(), kvsplit.next()) {
            env_var_map
                .insert(OsStr::from_bytes(k).to_os_string(), OsStr::from_bytes(v).to_os_string());
        };
    }

    Ok(env_var_map)
}

#[derive(Debug)]
pub struct DiscoveredProcess {
    // as discovered in env vars
    pub container_name: String,
    pub job_name: String,
    pub pid: i32,
    pub exec_id: String,
    pub start_time: SystemTime,
}

impl DiscoveredProcess {
    fn new(
        container_name: String, job_name: String, pid: i32, exec_id: String, start_timestamp: i64,
    ) -> Self {
        let start_time: SystemTime = DateTime::<Utc>::from_timestamp_millis(start_timestamp)
            .expect("Could not create datetime from timestamp")
            .into();
        Self { container_name, job_name, pid, exec_id, start_time }
    }

    pub fn matches_job(&self, job: &ExecJob) -> bool {
        job.job.name == self.job_name
            && *job.job.service_else_container_name() == self.container_name
    }
}

pub async fn discover_alfred_managed_processes(
    docker: Docker, container_id: &String,
) -> docker_api::Result<Vec<DiscoveredProcess>> {
    let inspect = docker.containers().get(container_id).inspect().await?;

    if inspect.exec_i_ds.is_none() {
        return Ok(vec![]);
    }

    let mut exec_list: Vec<DiscoveredProcess> = vec![];
    trace!("Inspecting container running processes: {}", container_id);
    let exec_ids = inspect.exec_i_ds.unwrap();
    for exec_id in exec_ids {
        let exec = docker_api::Exec::get(docker.clone(), &exec_id);
        let exec_inspect = match exec.inspect().await {
            Ok(exec_inspect) => exec_inspect,
            Err(err) => {
                warn!("Could not inspect exec instance {}: {}", exec_id, err);
                continue;
            }
        };

        match exec_inspect.running {
            None => {
                continue;
            }
            Some(running) => {
                if !running {
                    continue;
                }
            }
        };

        if exec_inspect.pid.is_none() {
            continue;
        }

        let pid = match exec_inspect.pid {
            None => continue,
            Some(pid) => pid,
        };

        if pid == 0 {
            // We see that docker shows a running exec with pid zero when it has just finished.
            // This is not documented from what we've seen.
            debug!("Got PID 0. Ignoring, assuming it is a recently-finished process");
            continue;
        }

        let mut env_vars = match get_pid_env_vars(pid as i32) {
            Ok(env_vars) => env_vars,
            Err(err) => {
                warn!("Could not read env vars for exec id {}: {}", exec_id, err);
                continue;
            }
        };

        let (Some(container_name), Some(job_name), Some(start_timestamp)) = (
            env_vars.remove(OsStr::new(ALFRED_MANAGED_CONTAINER_VARNAME)),
            env_vars.remove(OsStr::new(ALFRED_MANAGED_JOB_VARNAME)),
            env_vars.remove(OsStr::new(ALFRED_MANAGED_START_TIMESTAMP_VARNAME)),
        ) else {
            // nothing to do, pid does not have the necessary env vars
            continue;
        };

        let (Some(container_name), Some(job_name), Some(start_timestamp)) =
            (container_name.to_str(), job_name.to_str(), start_timestamp.to_str())
        else {
            warn!(
                "Error converting container and job name from os string to string: {:?} / {:?} / {:?}",
                container_name, job_name, start_timestamp
            );
            continue;
        };

        let start_timestamp = match start_timestamp.parse::<i64>() {
            Ok(start_timestamp) => start_timestamp,
            Err(err) => {
                warn!("Error converting start timestamp '{}' to i64: {:?}", start_timestamp, err);
                continue;
            }
        };

        exec_list.push(DiscoveredProcess::new(
            container_name.to_string(),
            job_name.to_string(),
            pid as i32,
            exec_id,
            start_timestamp,
        ));
    }

    Ok(exec_list)
}
