use chrono::{DateTime, Local};
use cron;
use duration_str::HumanFormat;
use std::fmt;
use std::str::FromStr;
use std::time::{Duration, SystemTime};

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Schedule {
    // For scheduling exactly exactly the time specified
    // by the cron schedule
    Cron(cron::Schedule),

    // The job will be run at this interval. If no-overlap is set,
    // and the previous job ran beyond the next scheduled run time,
    // it will be started immediately
    Every(Duration),
}

impl Schedule {
    // last_launch_time used for @every,
    // now used for the cron schedule
    pub fn upcoming_next(
        &self, last_launch_time: &SystemTime, now: &SystemTime,
    ) -> Option<DateTime<Local>> {
        match &self {
            Schedule::Cron(cron_schedule) => {
                cron_schedule.after(&Into::<DateTime<Local>>::into(*now)).next()
            }
            Schedule::Every(period) =>
            // unwrap() below will fail if we're beyond what system time can carry
            {
                Some(last_launch_time.checked_add(*period).unwrap().into())
            }
        }
    }
}

#[derive(Debug)]
pub enum Error {
    Cron(cron::error::Error),
    Every(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::Cron(error) => error.fmt(f),
            Error::Every(error) => error.fmt(f),
        }
    }
}

impl FromStr for Schedule {
    type Err = Error;

    // Strings are converted to jobs as show next:
    //   "@every <duration>": every schedule
    //      e.g. "@every 2m 3s"
    //   otherwise: cron schedule
    //      e.g. "0 */5 * * * *"
    fn from_str(expression: &str) -> Result<Self, Self::Err> {
        if expression.starts_with("@every ") {
            let (_, duration_slice) =
                expression.split_once(' ').expect("Expected space but found none");
            let trimmed = duration_slice.trim();

            match duration_str::parse(trimmed) {
                Ok(duration) => Ok(Schedule::Every(duration.into())),
                Err(err) => Err(Error::Every(err)),
            }
        } else {
            match cron::Schedule::from_str(expression) {
                Ok(schedule) => Ok(Schedule::Cron(schedule)), // Extract from nom tuple
                Err(err) => Err(Error::Cron(err)),
            }
        }
    }
}

impl fmt::Display for Schedule {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Schedule::Cron(schedule) => write!(f, "Cron {}", schedule),
            Schedule::Every(duration) => write!(f, "@every {}", duration.human_format()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{Datelike, Timelike};
    use more_asserts::*;
    use std::time::UNIX_EPOCH;

    #[test]
    fn from_str_parses_correctly_formatted_strings() {
        assert_eq!(
            Schedule::from_str("@every 20m 15s").unwrap(),
            Schedule::Every(Duration::from_secs(20 * 60 + 15))
        );

        assert_eq!(
            Schedule::from_str("*/3 * * * * *").unwrap(),
            Schedule::Cron(cron::Schedule::from_str("*/3 * * * * *").unwrap())
        );
    }

    #[test]
    fn from_str_every_fails_with_incorrect_format() {
        assert!(Schedule::from_str("@every xx").is_err());
        assert!(Schedule::from_str("@every ").is_err());
        assert!(Schedule::from_str("@every").is_err());

        assert!(Schedule::from_str("@every 10z").is_err());
    }

    #[test]
    fn upcoming_next_for_every_returns_correct_value() {
        let schedule = Schedule::from_str("@every 20m 15s").unwrap();

        let last_launch_time = SystemTime::now();

        // should be unused by the function below, as it is an 'every' job, and not 'cron'
        let now = last_launch_time.checked_sub(Duration::from_secs(1000000)).unwrap();

        let next = schedule.upcoming_next(&last_launch_time, &now).expect("Got unexpected None");

        let millis = next.timestamp_millis()
            - last_launch_time.duration_since(UNIX_EPOCH).unwrap().as_millis() as i64;
        assert_ge!(Duration::from_millis(millis as u64), Duration::from_secs(20 * 60 + 14));
        assert_le!(Duration::from_millis(millis as u64), Duration::from_secs(20 * 60 + 15));
    }

    #[test]
    fn upcoming_next_for_cron_returns_correct_value() {
        let schedule = Schedule::from_str("0 3 * * * *").unwrap();

        // should be unused by the call below, because this is a 'cron' and not an 'every' job
        let last_launch_time = SystemTime::now();
        let now = DateTime::from_timestamp(0, 0).unwrap().try_into().unwrap();
        let next = schedule.upcoming_next(&last_launch_time, &now).expect("Got unexpected None");

        assert_eq!(next.minute(), 3);
        assert_eq!(next.second(), 0);
        assert_eq!(next.day0(), 0);
        assert_eq!(next.month0(), 0);
        assert_eq!(next.year(), 1970);
    }
}
