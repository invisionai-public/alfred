use log::{info, warn};
use uuid::Uuid;

use clap::Parser;
use hostname;
use std::{collections::HashMap, time::UNIX_EPOCH};
use tokio::time::Duration;

mod job;

mod exec_job;
use exec_job::ExecJob;

mod schedule;

mod exec_discover;
mod future_utils;
use exec_discover::can_discover_existing_jobs;

mod exec_runner;
use exec_runner::run_scheduled;

mod docker_utils;
use docker_utils::{
    label_from_container_with_hostname, periodic_jobs_from_docker_labels, DockerLabelsInfo,
};

// Prometheus metrics
use lazy_static::lazy_static;
use prometheus::{register_gauge, register_int_gauge, Gauge, IntGauge};

lazy_static! {
    static ref METRIC_PROCESS_START_TIME: Gauge =
        register_gauge!("alfred_process_start_time", "Last time alfred process started").unwrap();
    static ref METRIC_CAN_DISCOVER_EXISTING_JOBS: IntGauge = register_int_gauge!(
        "alfred_can_discover_existing_jobs",
        "Non-zero if alfred process can detect existing jobs if it restarts"
    )
    .unwrap();
}

// Keep track of a job and associated task, if any
struct PeriodicJobAndTask {
    job: ExecJob,
    task: Option<tokio::task::JoinHandle<()>>,
}

impl PeriodicJobAndTask {
    fn new(job: ExecJob) -> PeriodicJobAndTask {
        PeriodicJobAndTask { job, task: None }
    }
}

#[derive(Parser, Debug)]
#[command(version, about="A cron-like scheduler for docker", long_about = None)]
struct CliArgs {
    #[arg(short, long,
        default_value_t = std::env::var("DOCKER_HOST").unwrap_or("unix:///var/run/docker.sock".into()),
        help="Default value read from DOCKER_HOST env var, if present."
    )]
    docker_socket: String,

    #[arg(short, long, default_value_t = 9184)]
    prometheus_port: u16,

    #[command(flatten)]
    docker_labels_info: DockerLabelsInfo,
}

#[tokio::main]
async fn main() {
    use env_logger::Env;
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let args = CliArgs::parse();

    let docker = docker_api::Docker::new(args.docker_socket).unwrap();

    // start prometheus exporter
    let binding = format!("0.0.0.0:{}", args.prometheus_port).parse().unwrap();
    prometheus_exporter::start(binding).unwrap();

    METRIC_CAN_DISCOVER_EXISTING_JOBS.set(can_discover_existing_jobs(docker.clone()).await as i64);
    info!("Can discover existing jobs: {}", METRIC_CAN_DISCOVER_EXISTING_JOBS.get());

    METRIC_PROCESS_START_TIME.set(
        std::time::SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .expect("Cannot compute process start time")
            .as_secs_f64(),
    );

    // find out if we run inside a docker container and the docker compose project name
    let hostname = hostname::get().expect("Could not get hostname");
    let compose_project_name = label_from_container_with_hostname(
        docker.clone(),
        hostname.to_str().unwrap(),
        &args.docker_labels_info.compose_project_name,
    )
    .await
    .expect("Could not get docker compose project from container running this process");

    match &compose_project_name {
        Some(name) => info!("Detected docker compose project: {}", name),
        None => info!("No docker compose project detected. Will apply to all containers"),
    }

    // stores jobs and task (if spawned already) for all jobs
    let mut running_jobs: HashMap<Uuid, PeriodicJobAndTask> = HashMap::new();

    // periodically update tasks to handle containers restarting
    loop {
        match periodic_jobs_from_docker_labels(
            docker.clone(),
            &compose_project_name,
            &args.docker_labels_info,
        )
        .await
        {
            Ok(mut parsed_jobs) => {
                let mut uuids_to_remove = vec![];
                let mut uuids_to_add = vec![];

                for (new_uuid, _) in parsed_jobs.iter() {
                    if !running_jobs.contains_key(new_uuid) {
                        uuids_to_add.push(*new_uuid);
                    }
                }

                for (cur_uuid, _) in running_jobs.iter() {
                    if !parsed_jobs.contains_key(cur_uuid) {
                        uuids_to_remove.push(*cur_uuid);
                    }
                }

                // remove jobs as needed and stop tasks
                for uuid in uuids_to_remove.iter() {
                    let job = running_jobs.remove(uuid).unwrap();
                    info!("Removing inexistent job: {}", job.job.to_string());
                    if job.task.is_some() {
                        job.task.unwrap().abort();
                    }
                }

                // add new jobs and start
                for uuid in uuids_to_add.iter() {
                    assert!(
                        !running_jobs.contains_key(uuid),
                        "Unexpected uuid {} in running_jobs",
                        *uuid
                    );
                    running_jobs
                        .insert(*uuid, PeriodicJobAndTask::new(parsed_jobs.remove(uuid).unwrap()));

                    match running_jobs.get_mut(uuid) {
                        Some(new_entry) => {
                            new_entry.task = Some(tokio::spawn(run_scheduled(
                                new_entry.job.clone(),
                                docker.clone(),
                            )));

                            info!(
                                "Registered new job: '{}' with schedule '{}'",
                                new_entry.job.to_string(),
                                new_entry.job.schedule
                            );
                        }
                        None => panic!("Unexpected existing map entry for job {}", uuid),
                    }
                }
            }
            Err(err) => warn!("Could not update jobs from labels: {}", err),
        }

        tokio::time::sleep(Duration::from_secs(1)).await;
    }
}
