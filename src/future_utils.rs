use std::pin::Pin;
use std::time::{SystemTime, UNIX_EPOCH};

use futures::future::select_all;
use futures::Future;
use itertools::Itertools;
use tokio::time::Instant;

type ExecId = String;

#[derive(PartialEq, Eq, Debug)]
pub enum FutureType {
    Task(ExecId),
    Timeout,
}

/// Handy to await on a Vec<FutureWithStartTime>
/// and keep track of the start time of each
pub struct FutureWithStartTime {
    future: Pin<Box<dyn Future<Output = ()> + Send>>,
    start_time: SystemTime,
    future_type: FutureType,
}

impl FutureWithStartTime {
    pub fn new(
        fut: Box<dyn Future<Output = ()> + Send>, future_type: FutureType, start_time: SystemTime,
    ) -> Self {
        Self { future: Box::into_pin(fut), start_time, future_type }
    }

    pub fn future_type(&self) -> &FutureType {
        return &self.future_type;
    }
}

impl Future for FutureWithStartTime {
    type Output = ();

    fn poll(
        mut self: std::pin::Pin<&mut Self>, cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Self::Output> {
        self.future.as_mut().poll(cx)
    }
}

/// Wait for whichever completes first, a future in `future_vec` or `deadline`
/// Returns (unfinished_futures: Vec, timedout: bool).
///
/// If `maybe_deadline` is None, it waits for a task to complete without any timeout.
///
/// Because of select_all(), we need to consume `futures_vec` and thus return a list
/// of futures that haven't finished yet
pub async fn select_with_timeout(
    futures_vec: Vec<FutureWithStartTime>, maybe_deadline: Option<Instant>,
) -> (Vec<FutureWithStartTime>, bool) {
    let num_to_await = futures_vec.len();

    let tasks_to_await = match maybe_deadline {
        Some(deadline) => select_all(futures_vec.into_iter().chain([FutureWithStartTime::new(
            Box::new(tokio::time::sleep_until(deadline)),
            FutureType::Timeout,
            SystemTime::now(),
        )])),
        None => select_all(futures_vec),
    };

    let (_, finished_idx, mut unfinished_futures) = tasks_to_await.await;

    // last item was timeout
    let timedout = finished_idx >= num_to_await;

    // select_all() returns a vec of futures with the future that completed removed
    if !timedout && maybe_deadline.is_some() {
        let sleep_idx = unfinished_futures
            .iter()
            .find_position(|&ft| ft.future_type == FutureType::Timeout)
            .expect("Could not find sleep task")
            .0;
        unfinished_futures.remove(sleep_idx);
    }

    (unfinished_futures, timedout)
}

pub fn get_oldest_start_time_or_zero(launched_tasks: &Vec<FutureWithStartTime>) -> f64 {
    if launched_tasks.is_empty() {
        return 0.0;
    } else {
        let oldest_entry = launched_tasks
            .iter()
            .filter(|&ft| matches!(*ft.future_type(), FutureType::Task(_)))
            .min_by_key(|&ft| ft.start_time)
            .expect("Could not get oldest start time");

        return oldest_entry
            .start_time
            .duration_since(UNIX_EPOCH)
            .expect("Cannot compute duration")
            .as_secs_f64();
    }
}
